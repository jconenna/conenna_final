﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    public GameObject[] panels;
    int page = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }



    public void ChangePage(int change)
    {
        panels[page].SetActive(false);

        page += change;

        if (page < 0)
        {
            page = panels.Length + page;
        }

        page = page % panels.Length;

        panels[page].SetActive(true);
    }
}


